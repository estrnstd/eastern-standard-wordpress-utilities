<?php
/*
Plugin Name: Eastern Standard WordPress Utilities
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities
Description: Helpful utilities for common tasks.
Version: 1.3.1
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-utilities
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

$eswp_dependency_manager_path = ABSPATH . 'wp-content/plugins/eswp-dependency-manager/eswp-dependency-manager.php';
if (file_exists($eswp_dependency_manager_path)) {
	include_once $eswp_dependency_manager_path;
}

if (!class_exists('ESWP_Utilities')) {
	#[AllowDynamicProperties]
	class ESWP_Utilities {

		public function __construct() {

			$this->file = __FILE__;
			$this->dir = __DIR__;
			$this->plugin_data = get_plugin_data(__FILE__);
			$this->slug = plugin_basename(__DIR__);
			$this->basename_path = $this->slug . '/' . $this->slug . '.php';
			$this->dashboard_updating = true;
			$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities/raw/v1/info.json';
			$this->info_cache_allowed = true;
			$this->info_cache_key = $this->slug . '-info';
			$this->info_cache_length = DAY_IN_SECONDS;
			$this->plugin_dependencies = [
				[
					'name' => 'Advanced Custom Fields Pro', // Optional. Readable name of the plugin.
					'url' => 'https://www.advancedcustomfields.com/pro', // Optional. Link to the plugin.
					'path' => 'advanced-custom-fields-pro/acf.php', // Required. Path to the plugin relative to the plugins directory.
					'version' => '>=5.8', // Required. Semver constraints.
				],
			];
			
			if (class_exists('ESWP_Updater')) {
				ESWP_Updater::initialize_plugin_updates($this);
			}
			else {
				add_action('plugin-eswp-updater:initialized', function() {
					ESWP_Updater::initialize_plugin_updates($this);
				});
			}

			if (class_exists('ESWP_Dependency_Manager')) {
				$valid_dependencies = ESWP_Dependency_Manager::check_plugin_dependencies($this);
			}

		}

	}

	new ESWP_Utilities();

}


/**
 * eswp_dump
 * 
 * Debugging helper function.
 * 
 * @param   mixed  $values  Any number of values.
 * @return  void
 */

if (!function_exists('eswp_dump')) {
	function eswp_dump(...$values) {
		echo '<pre>';var_dump(...$values);echo '</pre>';
	}
}


/**
 * Polyfills for PHP versions < 8
 */

if (!function_exists('str_starts_with')) {
	function str_starts_with($haystack, $needle) {
		$length = strlen($needle);
		return substr($haystack, 0, $length) === $needle;
	}
}

if (!function_exists('str_ends_with')) {
	function str_ends_with($haystack, $needle) {
		$length = strlen($needle);
		if (!$length) {
			return true;
		}
		return substr($haystack, -$length) === $needle;
	}
}

if (!function_exists('str_contains')) {
	function str_contains($haystack, $needle) {
		return $needle !== '' && mb_strpos($haystack, $needle) !== false;
	}
}


/**
 * eswp_get_public_path
 * 
 * Get the public version of an internal path. Will return the unedited input path if it was not found to be an internal path.
 * 
 * @param   string  $internal_path  An internal path.
 * @return  string  A public version of the internal path.
 */

if (!function_exists('eswp_get_public_path')) {
	function eswp_get_public_path($internal_path) {
		$public_path = $internal_path;

		$internal_path_root = $_SERVER['DOCUMENT_ROOT'];
		if (str_ends_with($internal_path_root, '.')) {
			$internal_path_root = substr($internal_path_root, 0, -1);
		}

		// Convert Windows path backslashes to regular slashes
		$internal_path_root = str_replace('\\', '/', $internal_path_root);
		$internal_path = str_replace('\\', '/', $internal_path);

		$internal_path_root_position = strpos($internal_path, $internal_path_root);
		if ($internal_path_root_position === 0) {
			$public_path = substr_replace($internal_path, '', 0, strlen($internal_path_root));
		}

		if (!str_starts_with($public_path, '/')) {
			$public_path = '/' . $public_path;
		}

		return $public_path;
	}
}

/**
 * eswp_get_internal_path
 * 
 * Get the internal version of a public path. Will return the unedited input path if it was not found to be a public path.
 * 
 * @param   string  $public_path  A public path.
 * @return  string  An internal version of the public path.
 */

 if (!function_exists('eswp_get_internal_path')) {
	function eswp_get_internal_path($public_path) {
		$internal_path = $public_path;

		if (str_starts_with($internal_path, 'http://')) {
			$internal_path = substr($internal_path, strlen('http://'));
		}

		if (str_starts_with($internal_path, 'https://')) {
			$internal_path = substr($internal_path, strlen('https://'));
		}

		if (str_starts_with($internal_path, '//')) {
			$internal_path = substr($internal_path, strlen('//'));
		}

		$public_path_root = $_SERVER['HTTP_HOST'];
		if (str_ends_with($public_path_root, '/')) {
			$public_path_root = substr($public_path_root, 0, -1);
		}
		if (str_starts_with($internal_path, $public_path_root)) {
			$internal_path = substr($internal_path, strlen($public_path_root));
		}

		if (!str_starts_with($internal_path, '/')) {
			if (isset($_SERVER['REQUEST_URI'])) {
				$internal_path = $_SERVER['REQUEST_URI'] . $internal_path;
			}
			if (!str_starts_with($internal_path, '/')) {
				$internal_path = '/' . $internal_path;
			}
		}

		$internal_path_root = $_SERVER['DOCUMENT_ROOT'];
		if (str_ends_with($internal_path_root, '.')) {
			$internal_path_root = substr($internal_path_root, 0, -1);
		}
		if (str_ends_with($internal_path_root, '/')) {
			$internal_path_root = substr($internal_path_root, 0, -1);
		}

		$internal_path = $internal_path_root . $internal_path;

		return $internal_path;
	}
}


/**
 * eswp_get_excerpt
 * 
 * Returns an excerpt from specified block fields.
 * 
 * @param  array  $options  An array of options.
 * |- 'post'             (object|int)  Required. The post object or post id where to search for the values.
 * |- 'parsed_blocks'    (array)       Optional. Typically for internal use only.
 * |- 'block_fields'     (array)       Required. An array of block names and the fields to get data from.
 * |- 'character_count'  (int)         Optional. The character count to trim the excerpt to.
 * |- 'field_separator'  (string)      Optional. The separator between field values.
 * @return  string  An excerpt string.
 */

if (!function_exists('eswp_get_excerpt')) {
	function eswp_get_excerpt($options) {

		$default_options = [
			'parsed_blocks' => null,
			'character_count' => 400,
			'field_separator' => "\n",
		];
		$options = array_merge($default_options, $options);

		if (!array_key_exists('post', $options) || !is_object($options['post']) && !is_int($options['post'])) {
			trigger_error(__("Function 'eswp_get_excerpt' expects the 'post' option to be a post object or a post id."), E_USER_WARNING);
		}
		if (is_int($options['post'])) {
			$options['post'] = get_post($options['post']);
		}
		$options['parsed_blocks'] = eswp_get_parsed_blocks([
			'post' => $options['post'],
			'parsed_blocks' => $options['parsed_blocks'],
		]);

		$excerpt = '';

		// for each parsed block
		foreach ($options['parsed_blocks'] as $parsed_block) {
			// if the block type has excerpt fields defined
			if (array_key_exists($parsed_block['blockName'], $options['block_fields'])) {
				// check if the block has ACF data
				if (array_key_exists('attrs', $parsed_block) && is_array($parsed_block['attrs'])) {
					if (array_key_exists('data', $parsed_block['attrs']) && is_array($parsed_block['attrs']['data'])) {
						$excerpt_fields = $options['block_fields'][$parsed_block['blockName']];
						if (strlen($excerpt) > 0) {
							$excerpt .= $options['field_separator'];
						}
						$excerpt .= eswp_get_excerpt_from_block([
							'parsed_block' => $parsed_block,
							'excerpt_fields' => $excerpt_fields,
							'field_separator' => $options['field_separator'],
						]);
					}
				}
			}
		}

		$excerpt = strip_tags($excerpt);
		$excerpt = trim($excerpt);
		if (strlen($excerpt) > $options['character_count']) {
			$excerpt = substr($excerpt, 0, $options['character_count']);
			$excerpt .= '...';
		}

		return $excerpt;

	}
}

if (!function_exists('eswp_get_block_excerpt')) {
	function eswp_get_excerpt_from_block($options) {
		$default_options = [
			'sub_fields' => false,
		];
		$options = array_merge($default_options, $options);
		$block_excerpt = '';
		if (!$options['sub_fields']) {
			$meta_id = eswp_setup_acf_block_meta($options['parsed_block']);
		}
		foreach ($options['excerpt_fields'] as $excerpt_fields_key => $excerpt_fields_value) {
			if (is_string($excerpt_fields_value)) {
				if ($options['sub_fields']) {
					$field_value = get_sub_field($excerpt_fields_value);
				}
				else {
					$field_value = get_field($excerpt_fields_value, $meta_id);
				}
				if (is_int($field_value) || is_float($field_value)) {
					$field_value = strval($field_value);
				}
				if (is_string($field_value)) {
					if (strlen($block_excerpt) > 0) {
						$block_excerpt .= $options['field_separator'];
					}
					$block_excerpt .= $field_value;
				}
			}
			else if (is_array($excerpt_fields_value)) {
				if (have_rows($excerpt_fields_key, $meta_id)) {
					while (have_rows($excerpt_fields_key, $meta_id)) {
						the_row();
						$inner_options = $options;
						$inner_options['excerpt_fields'] = $excerpt_fields_value;
						$inner_options['sub_fields'] = true;
						$inner_excerpt = eswp_get_excerpt_from_block($inner_options);
						if (strlen($inner_excerpt) > 0) {
							if (strlen($block_excerpt) > 0) {
								$block_excerpt .= $options['field_separator'];
							}
							$block_excerpt .= $inner_excerpt;
						}
					}
				}
			}
		}
		if (!$options['sub_fields']) {
			acf_reset_meta($meta_id);
		}
		return $block_excerpt;
	}
}


/**
 * eswp_menu_item_has_parent
 */

if (!function_exists('eswp_menu_item_has_parent')) {
	function eswp_menu_item_has_parent($menu_items, $menu_item) {
		if (isset($menu_item->menu_item_parent) && intval($menu_item->menu_item_parent) > 0) {
			return true;
		}
		else {
			return false;
		}
	}
}


/**
 * eswp_get_menu_item_by_id
 */
if (!function_exists('eswp_get_menu_item_by_id')) {
	function eswp_get_menu_item_by_id($menu_items, $menu_item_id) {
		$item = null;
		foreach ($menu_items as $menu_item) {
			if ($menu_item->ID === $menu_item_id) {
				$item = $menu_item;
				break;
			}
		}
		return $item;
	}
}


/**
 * eswp_get_menu_item_depth_from_root
 */
if (!function_exists('eswp_get_menu_item_depth_from_root')) {
	function eswp_get_menu_item_depth_from_root($menu_items, $root_menu_item, $menu_item) {
		$depth = 0;
		$found_root = false;
		while ($menu_item && !$found_root) {
			$depth++;
			if (eswp_menu_item_has_parent($menu_items, $menu_item)) {
				$parent_menu_item = eswp_get_menu_item_by_id($menu_items, intval($menu_item->menu_item_parent));
				if ($parent_menu_item) {
					$menu_item = $parent_menu_item;
					if (is_object($parent_menu_item) && $parent_menu_item->ID === $root_menu_item->ID) {
						$found_root = true;
					}
					continue;
				}
			}
			$menu_item = false;
		}
		if (!$found_root) {
			$depth = null;
		}
		return $depth;
	}
}


/**
 * eswp_attributes_has_class_name
 * 
 * Checks if a specific class name exists within an attributes array.
 *
 * @param   string[]  $attributes  An array of attributes and values.
 * @param   string    $class_name  The class name to search for.
 * @return  boolean                The result of the search for the class name.
 */
if (!function_exists('eswp_attributes_has_class_name')) {
	function eswp_attributes_has_class_name($attributes, $class_name) {
		if (!array_key_exists('class', $attributes)) {
			return false;
		}
		$classes = explode(' ', $attributes['class']);
		if (in_array($class_name, $classes)) {
			return true;
		}
		else {
			return false;
		}
	}
}


/**
 * eswp_attributes_add_class_name
 * 
 * Adds a class name(s) to an attributes array. Does not edit the input attributes, returns an updated new attributes array.
 *
 * @param   string[]  $attributes   An array of attributes and values.
 * @param   mixed     $class_names  The class name(s) to add to the attributes. Can be a string or an array of strings.
 * @return  string[]                The updated version of the attributes.
 */

if (!function_exists('eswp_attributes_add_class_name')) {
	function eswp_attributes_add_class_name($attributes, $class_names = []) {
		if (is_array($class_names)) {
			foreach ($class_names as $class_name) {
				$attributes = eswp_attributes_add_class_name($attributes, $class_name);
			}
		}
		else if (is_string($class_names)) {
			$class_names = explode(' ', $class_names);
			if (count($class_names) > 1) {
				$attributes = eswp_attributes_add_class_name($attributes, $class_name);
			}
			else {
				$class_name = $class_names[0];
				if (!eswp_attributes_has_class_name($attributes, $class_name)) {
					if (!array_key_exists('class', $attributes)) {
						$attributes['class'] = '';
					}
					if (strlen($attributes['class']) > 0) {
						$attributes['class'] .= ' ';
					}
					$attributes['class'] .= $class_name;
				}
			}
		}
		else {
			throw new Error(__("'class_names' should be a string or an array of strings."));
		}
		return $attributes;
	}
}


/**
 * eswp_attributes_add_class_names
 * 
 * Plural alias of eswp_attributes_add_class_name.
 */

if (!function_exists('eswp_attributes_remove_class_name')) {
	function eswp_attributes_remove_class_name($attributes, $class_names = []) {
		return eswp_attributes_add_class_name($attributes, $class_names);
	}
}


/**
 * eswp_attributes_remove_class_name
 * 
 * Removes a class name(s) from an attributes array. Does not edit the input attributes, returns an updated new attributes array.
 *
 * @param   string[]  $attributes   An array of attributes and values.
 * @param   mixed     $class_names  The class name(s) to remove from the attributes. Can be a string or an array of strings.
 * @return  string[]                The updated version of the attributes.
 */

if (!function_exists('eswp_attributes_remove_class_name')) {
	function eswp_attributes_remove_class_name($attributes, $class_names = []) {
		if (!is_array($class_names)) {
			$class_names = explode(' ', $class_names);
		}
		if (count($class_names) > 1) {
			foreach ($class_names as $class_name) {
				$attributes = eswp_attributes_remove_class_name($attributes, $class_name);
			}
		}
		else {
			$class_name = $class_names[0];
			if (eswp_attributes_has_class_name($attributes, $class_name)) {
				$attribute_class_names = explode(' ', $attributes['class']);
				$class_name_index = array_search($class_name, $attribute_class_names);
				if ($class_name_index >= 0) {
					unset($attribute_class_names[$class_name_index]);
				}
				if (count($attribute_class_names) < 1) {
					unset($attributes['class']);
				}
			}
		}
		return $attributes;
	}
}


/**
 * eswp_attributes_remove_class_names
 * 
 * Plural alias of eswp_attributes_remove_class_name.
 */
if (!function_exists('eswp_attributes_remove_class_names')) {
	function eswp_attributes_remove_class_names($attributes, $class_names = []) {
		return eswp_attributes_remove_class_name($attributes, $class_names);
	}
}


/**
 * eswp_output_attributes
 * 
 * Returns a string of formatted attributes ready to be added to an HTML element.
 *
 * @param   string[]  $attributes  An array of attributes and values.
 * @return  string                 The output of formatted attributes.
 */

if (!function_exists('eswp_output_attributes')) {
	function eswp_output_attributes($attributes) {
		$output = '';
		foreach ($attributes as $name => $value) {
			$output .= ' ' . $name . '="' . $value . '"';
		}
		return $output;
	}
}


/**
 * eswp_define_image_sizes
 * 
 * Adds image sizes in a normalized manner.
 * https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * @param   string[]  $image_sizes  An array of image size options.
 * @return  void
 */

if (!function_exists('eswp_define_image_sizes')) {
	function eswp_define_image_sizes($image_sizes = []) {
		foreach ($image_sizes as $image_size) {
			add_image_size($image_size['name'], $image_size['width'], $image_size['height'], $image_size['crop']);
			if (isset($image_size['pixel_densities']) && is_array($image_size['pixel_densities'])) {
				foreach ($image_size['pixel_densities'] as $pixel_density) {
					$name = $image_size['name'] . '-' . strval($pixel_density) . 'x';
					$width = $image_size['width'] * $pixel_density;
					$height = $image_size['height'] * $pixel_density;
					add_image_size($name, $width, $height, $image_size['crop']);
				}
			}
		}
	}
}


/**
 * eswp_format_acf_picture
 * 
 * Returns a HTML string of a picture element constructed from Advanced Custom Field sources.
 *
 * @param  array  $sources  Required. An array of arrays including image sources and media queries.
 * |- [                         (array)
 * |- |- 'field'            =>  (mixed)    Required. The field key/name string or field object array to get the value from.
 * |- |- 'is_sub_field'     =>  (boolean)  Optional. Defaults to false.
 * |- |- 'block_name'       =>  (string)   Optional. The name of the block containing the field.
 * |- |- 'post'             =>  (mixed)    Optional. The post object or post id where to search for the values.
 * |- |- 'size'             =>  (string)   Required. The size the source to output.
 * |- |- 'media'            =>  (string)   Optional. The media query value for the source.
 * |- |- 'pixel_densities'  =>  (int[])    Optional. Pixels densities to output.
 * |- |- 'fallback'         =>  (bool)     Optional. If true, this source will be the image chosen for the img tag.
 * |- ],
 * |- ...
 * @param  array  $options  Optional. An array of options.
 * |- 'picture'  =>  (array)  Optional.
 * |- |- 'attributes'  =>  (array)  Optional. Attributes for the picture element.
 * |- img'  =>  (array)  Optional.
 * |- |- 'attributes'  =>  (array)  Optional. Attributes for the img element.
 * @return  string  The picture element output.
 */

if (!function_exists('eswp_format_acf_picture')) {
	function eswp_format_acf_picture($sources = [], $options = []) {
		if (!function_exists('get_field_object') || !function_exists('get_sub_field_object')) {
			throw new Error(__("Function 'eswp_format_acf_picture' requires the plugin 'Advanced Custom Fields (ACF)'. Please make sure ACF is installed and activated before using the 'eswp_format_acf_picture' function."));
		}

		global $post;
		global $_wp_additional_image_sizes;
		$new_line_character = "\r\n";

		$picture_attributes = [];
		if (is_array($options) && array_key_exists('picture', $options)
		&& is_array($options['picture']) && array_key_exists('attributes', $options['picture'])
		&& is_array($options['picture']['attributes'])) {
			$picture_attributes = $options['picture']['attributes'];
		}
		$output = '<picture' . eswp_output_attributes($picture_attributes) . '>' . $new_line_character;
		$fallback_source = null;

		if (isset($sources) && is_array($sources) && count($sources) > 0) {
			foreach($sources as &$source) {

				$source_attributes = [];
				if (isset($source['field'])
				&& (is_string($source['field']) || is_array($source['field']))) {

					if (is_array($source['field'])) {
						$field_object = $source['field'];
					}
					else if (is_string($source['field'])) {
						$source_post_id = null;
						if (isset($source['post'])) {
							if (is_object($source['post'])) {
								$source_post_id = $source['post']->ID;
							}
							else if (is_int($source['post'])) {
								$source_post_id = $source['post'];
							}
						}

						$source_is_sub_field = false;
						if (isset($source['is_sub_field'])) {
							$source_is_sub_field = $source['is_sub_field'];
						}

						$field_object = null;
						if (isset($source['block_name']) && is_string($source['block_name'])) {
							$field_object = eswp_get_block_field([
								'field' => $source['field'],
								'is_sub_field' => $source_is_sub_field,
								'block_name' => $source['block_name'],
								'post' => $source_post_id,
								'first_match' => true,
								'return_field_object' => true,
							]);
						}
						else {
							if ($source_is_sub_field) {
								$field_object = get_sub_field_object($source['field'], $source_post_id);
							}
							else {
								$field_object = get_field_object($source['field'], $source_post_id);
							}
						}
					}

					$size_name = null;
					if (isset($source['size']) && is_string($source['size'])) {
						$size_name = $source['size'];
					}

					// with size
					if ($field_object
					&& isset($size_name)
					&& isset($field_object['value'])
					&& (
						(
							is_array($field_object['value'])
							&& isset($field_object['value']['sizes']) && is_array($field_object['value']['sizes'])
						)
						|| is_int($field_object['value'])
					)) {
						$source_attributes['data-image-size'] = $size_name;
						$srcset_output = '';
						$size_width = null;
						$size_height = null;
						if (is_array($field_object['value'])) {
							if (isset($field_object['value']['sizes'][$size_name])) {
								$source['src_output'] = $field_object['value']['sizes'][$size_name];
								$srcset_output .= $source['src_output'];
							}
							if (isset($field_object['value']['sizes'][$size_name . '-width'])) {
								$size_width = $field_object['value']['sizes'][$size_name . '-width'];
							}
							if (isset($field_object['value']['sizes'][$size_name . '-height'])) {
								$size_height = $field_object['value']['sizes'][$size_name . '-height'];
							}
						}
						/*
							For some reason get_sub_field_object() doesn't respect the 'return_format' setting in the field config. Instead it always outputs the image id (int).
						*/
						else if (is_int($field_object['value'])) {
							$source_attachment_image_src = wp_get_attachment_image_src($field_object['value'], $size_name);
							$source['src_output'] = $source_attachment_image_src[0];
							$srcset_output = $source['src_output'];
							$size_width = $source_attachment_image_src[1];
							$size_height = $source_attachment_image_src[2];
						}

						if (isset($source['pixel_densities']) && is_array($source['pixel_densities']) &&
						isset($size_width) && isset($size_height)) {
							foreach($source['pixel_densities'] as $pixel_density) {

								$pixel_density_size_name = $size_name . '-' . strval($pixel_density) . 'x';
								$pixel_density_size_url = null;
								$pixel_density_size_target_width = intval($size_width * $pixel_density);
								$pixel_density_size_target_height = intval($size_height * $pixel_density);
								$pixel_density_size_width = null;
								$pixel_density_size_height = null;

								if (is_array($field_object['value'])) {
									if (isset($field_object['value']['sizes'][$pixel_density_size_name])
									&& isset($field_object['value']['sizes'][$pixel_density_size_name . '-width'])
									&& isset($field_object['value']['sizes'][$pixel_density_size_name . '-height'])) {
										$pixel_density_size_url = $field_object['value']['sizes'][$pixel_density_size_name];
										$pixel_density_size_width = $field_object['value']['sizes'][$pixel_density_size_name . '-width'];
										$pixel_density_size_height = $field_object['value']['sizes'][$pixel_density_size_name . '-height'];
									}
								}
								else if (is_int($field_object['value'])) {
									$pixel_density_attachment_image_src = wp_get_attachment_image_src($field_object['value'], $pixel_density_size_name);
									$pixel_density_size_url = $pixel_density_attachment_image_src[0];
									$pixel_density_size_width = $pixel_density_attachment_image_src[1];
									$pixel_density_size_height = $pixel_density_attachment_image_src[2];
								}

								if (isset($pixel_density_size_url)
								&& isset($pixel_density_size_width)
								&& isset($pixel_density_size_height)) {
									if ($pixel_density_size_width === $pixel_density_size_target_width &&
									$pixel_density_size_height === $pixel_density_size_height) {
										$srcset_output .= ', ' . $pixel_density_size_url . ' ' . strval($pixel_density) . 'x';
									}
								}

							}
						}

						$source_attributes['srcset'] = $srcset_output;
					}
					// without size
					else if ($field_object
					&& isset($field_object['value'])
					&& (
						(
							is_array($field_object['value'])
							&& isset($field_object['value']['url'])
						)
						|| is_int($field_object['value'])
					)) {
						if (is_array($field_object['value'])) {
							$source['src_output'] = $field_object['value']['url'];
							$source_attributes['srcset'] = $source['src_output'];
						}
						else if (is_int($field_object['value'])) {
							$source_attachment_image_src = wp_get_attachment_image_src($field_object['value']);
							$source['src_output'] = $source_attachment_image_src[0];
							$source_attributes['srcset'] = $source['src_output'];
						}
					}
					// placeholder
					else {
						$placeholder_url = eswp_get_public_path(WP_PLUGIN_DIR . '/eswp-utilities/missing-image.svg.php');
						$placeholder_width = null;
						$placeholder_height = null;
						if (isset($size_name)
						&& isset($_wp_additional_image_sizes) && is_array($_wp_additional_image_sizes)
						&& isset($_wp_additional_image_sizes[$size_name])	&& is_array($_wp_additional_image_sizes[$size_name])
						&& isset($_wp_additional_image_sizes[$size_name]['width'])
						&& isset($_wp_additional_image_sizes[$size_name]['height'])) {
							$placeholder_width = $_wp_additional_image_sizes[$size_name]['width'];
							$placeholder_height = $_wp_additional_image_sizes[$size_name]['height'];
						}
						if (isset($placeholder_width) && isset($placeholder_height)) {
							$placeholder_url .= '?width=' . strval($placeholder_width) . '&height=' . strval($placeholder_height);
						}
						$source['src_output'] = $placeholder_url;
						$source_attributes['srcset'] = $source['src_output'];
						$source_attributes['type'] = 'image/svg+xml';
					}
					// media
					if (isset($source['media'])) {
						$source_attributes['media'] = $source['media'];
					}
					// type
					if (isset($source['src_output']) && !isset($source_attributes['type'])) {
						$path_parts = pathinfo($source['src_output']);
						if (isset($path_parts['extension'])) {
							if (substr($path_parts['extension'], 0, 3) === 'jpg' || substr($path_parts['extension'], 0, 4) === 'jpeg') {
								$source_attributes['type'] = 'image/jpeg';
							}
							else if (substr($path_parts['extension'], 0, 3) === 'png') {
								$source_attributes['type'] = 'image/png';
							}
							else if (substr($path_parts['extension'], 0, 3) === 'svg') {
								$source_attributes['type'] = 'image/svg+xml';
							}
							else if (substr($path_parts['extension'], 0, 3) === 'gif') {
								$source_attributes['type'] = 'image/gif';
							}
							else if (substr($path_parts['extension'], 0, 4) === 'webp') {
								$source_attributes['type'] = 'image/webp';
							}
						}
					}

				}
				else {
					continue;
				}

				if (isset($source['fallback']) && $source['fallback'] === true) {
					$fallback_source = $source;
				}
				if (isset($source_attributes['srcset'])) {
					$output .= '<source' . eswp_output_attributes($source_attributes) . '>' . $new_line_character;
				}

			}
			if (!isset($fallback_source)) {
				$fallback_source = $sources[0];
			}
			// image tag/fallback
			if (isset($fallback_source)) {
				$img_attributes = [];
				$alt = '';
				if (isset($field_object['value']) && is_array($field_object['value']) &&
				isset($field_object['value']['original_image']) && is_array($field_object['value']['original_image']) &&
				isset($field_object['value']['original_image']['alt']) && is_string($field_object['value']['original_image']['alt'])) {
					$alt = $field_object['value']['original_image']['alt'];
				}
				if (isset($field_object['value']['alt'])
				&& is_string($field_object['value']['alt'])
				&& strlen($field_object['value']['alt']) > 0) {
					$alt = $field_object['value']['alt'];
				}
				if (isset($alt)) {
					$img_attributes['alt'] = $alt;
				}
				if (isset($fallback_source['src_output'])) {
					$img_attributes['src'] = $fallback_source['src_output'];
				}
				if (is_array($options) && array_key_exists('img', $options)
				&& is_array($options['img']) && array_key_exists('attributes', $options['img'])
				&& is_array($options['img']['attributes'])) {
					$img_attributes = array_merge($img_attributes, $options['img']['attributes']);
				}
				$output .= '<img' . eswp_output_attributes($img_attributes) . '>' . $new_line_character;
			}
		}
		else {
			throw new Error(__("Function 'eswp_format_acf_picture' requires the '\$sources' argument to be an array of sources."));
		}
		$output .= '</picture>';
		return $output;
	}
}

/**
 * eswp_get_parsed_blocks
 * 
 * Returns an array of blocks matching parameters in the options.
 * 
 * @param  mixed[]  $options  An array of options.
 * |- 'post'           =>  (object|int)    Optional. The post object or post id to get the blocks from.
 * |- 'name'           =>  (string|array)  Optional. String or array of strings to filter the blocks by.
 * |- 'parsed_blocks'  =>  (array)         Optional. Pre-supply the array of parsed blocks.
 * |- 'first_match'    =>  (boolean)       Optional. Defaults to false. If true, will return the first matching block.
 * @return  array  An array containing matching blocks.
 */

if (!function_exists('eswp_get_parsed_blocks')) {
	function eswp_get_parsed_blocks($options = []) {
		global $post;
		$return_blocks = [];

		// post
		if (isset($options['post'])) {
			if (!is_object($options['post']) && !is_int($options['post'])) {
				trigger_error(__("Function 'eswp_get_parsed_blocks', argument '\$options['post']' must be a post (object), post id (int), or unset."), E_USER_WARNING);
				return;
			}
			if (is_int($options['post'])) {
				$options['post'] = get_post($options['post']);
				if (!is_object($options['post'])) {
					trigger_error(__("Function 'eswp_get_parsed_blocks', argument '\$options['post']', found no post object for value {$options['post']}."), E_USER_WARNING);
					return;
				}
			}
		}
		else {
			$options['post'] = $post;
		}

		// name
		if (isset($options['name'])) {
			$is_option_name_valid = true;
			if (is_string($options['name'])) {
				$options['name'] = [$options['name']];
			}
			if (is_array($options['name'])) {
				foreach ($options['name'] as $name) {
					if (!is_string($name)) {
						$is_option_name_valid = false;
						break;
					}
				}
			}
			if (!$is_option_name_valid) {
				trigger_error(__("Function 'eswp_get_parsed_blocks', argument '\$options['name']' must be a string, an array of strings, or unset."), E_USER_WARNING);
				return;
			}
		}

		// parsed_blocks
		if (isset($options['parsed_blocks']) && !empty($options['parsed_blocks'])) {
			if (!is_array($options['parsed_blocks'])) {
				trigger_error(__("Function 'eswp_get_parsed_blocks', argument '\$options['parsed_blocks']' must be a an array or unset."), E_USER_WARNING);
				return;
			}
		}
		else {
			$options['parsed_blocks'] = parse_blocks($options['post']->post_content);
		}

		// first_match
		if (isset($options['first_match'])) {
			if (!is_bool($options['first_match'])) {
				trigger_error(__("Function 'eswp_get_parsed_blocks', argument '\$options['first_match']' must be a boolean or unset."), E_USER_WARNING);
				return;
			}
		}
		else {
			$options['first_match'] = false;
		}

		if (is_array($options['parsed_blocks'])) {
			foreach ($options['parsed_blocks'] as $parsed_block) {
				if (isset($options['name']) && is_array($options['name'])) {
					foreach ($options['name'] as $options_name) {
						if (is_string($options_name) && $options_name === $parsed_block['blockName']) {
							if ($options['first_match']) {
								return $parsed_block;
							}
							else {
								array_push($return_blocks, $parsed_block);
							}
						}
					}
				}
				else {
					if ($options['first_match']) {
						return $parsed_block;
					}
					else {
						array_push($return_blocks, $parsed_block);
					}
				}
				if (is_array($parsed_block['innerBlocks']) && count($parsed_block['innerBlocks']) > 0) {
					$inner_blocks_options = $options;
					$inner_blocks_options['parsed_blocks'] = $parsed_block['innerBlocks'];
					$returned_inner_blocks = eswp_get_parsed_blocks($inner_blocks_options);
					if ($returned_inner_blocks) {
						if ($options['first_match']) {
							if ($inner_blocks_options['first_match']) {
								return $returned_inner_blocks;
							}
							else {
								return $returned_inner_blocks[0];
							}
						}
						else {
							if ($inner_blocks_options['first_match']) {
								array_push($return_blocks, $returned_inner_blocks);
							}
							else {
								foreach ($returned_inner_blocks as $returned_inner_block) {
									array_push($return_blocks, $returned_inner_block);
								}
							}
						}
					}
				}
			}
		}

		return $return_blocks;
	}
}

if (!function_exists('eswp_get_parsed_block')) {
	function eswp_get_parsed_block($options = []) {
		$options['first_match'] = true;
		return eswp_get_parsed_blocks($options);
	}
}

/**
 * eswp_get_parsed_block_acf_id
 * 
 * Returns the block id of the parsed block.
 * 
 * @param  array  $parsed_block  A parsed ACF block.
 * @return  string  An ACF block id.
 */

if (!function_exists('eswp_get_parsed_block_acf_id')) {
	function eswp_get_parsed_block_acf_id($parsed_block) {
		$acf_block_id = null;
		if (is_array($parsed_block['attrs'])) {
			// ACF version >= 6.0
			if (function_exists('acf_get_block_id') && function_exists('acf_ensure_block_id_prefix')) {
				$acf_block_id = acf_get_block_id($parsed_block['attrs']);
				$acf_block_id = acf_ensure_block_id_prefix($acf_block_id);
			}
			// ACF version < 6.0
			else if (array_key_exists('id', $parsed_block['attrs'])) {
				$acf_block_id = $parsed_block['attrs']['id'];
			}
		}
		return $acf_block_id;
	}
}

/**
 * 
 */

function eswp_setup_acf_block_meta($parsed_block, $meta_id = null) {
	if (is_null($meta_id)) {
		$meta_id = eswp_get_parsed_block_acf_id($parsed_block);
		if (!is_string($meta_id) || strlen($meta_id) <= 0) {
			$meta_id = 'temp_id_' . uniqid();
		}
	}
	acf_setup_meta($parsed_block['attrs']['data'], $meta_id);
	return $meta_id;
}

/**
 * eswp_get_block_field
 * 
 * Returns block ACF field values based on passed options.
 * 
 * @param  array  $options  An array of options.
 * |- 'field'                =>  (string)      Required. The field key/name to get the value from.
 * |- 'block_name'           =>  (string)      Optional. The name of the block containing the field.
 * |- 'post'                 =>  (object|int)  Optional. The post object or post id where to search for the values.
 * |- 'first_match'          =>  (boolean)     Optional. If true, will return the first found value directly.
 * |- 'is_sub_field'         =>  (boolean)     Optional. Defaults to false.
 * |- 'return_field_object'  =>  (boolean)     Optional. If true, will return the field object(s) instead of the field value.
 * |- 'parsed_blocks'        =>  (array)       Optional. Scope field values to those found in these parsed blocks.
 * @return  mixed  An array containing field values. If first_match is set to true, the first matching field value or null.
 */

if (!function_exists('eswp_get_block_field')) {
	function eswp_get_block_field($options) {
		global $post;
		$found_values = [];
		$eswp_get_parsed_blocks_options = [];

		// field
		if (!is_string($options['field'])) {
			trigger_error(__("Function 'eswp_get_block_field', argument '\$options['field']' must be a string."), E_USER_WARNING);
			return;
		}

		// block_name
		if (isset($options['block_name'])) {
			if (!is_string($options['block_name'])) {
				trigger_error(__("Function 'eswp_get_block_field', argument '\$options['block_name']' must be a string or unset."), E_USER_WARNING);
				return;
			}
			$eswp_get_parsed_blocks_options['name'] = $options['block_name'];
		}

		// post
		if (isset($options['post'])) {
			if (!is_object($options['post']) && !is_int($options['post'])) {
				trigger_error(__("Function 'eswp_get_block_field', argument '\$options['post']' must be a post (object), post id (int), or unset."), E_USER_WARNING);
				return;
			}
			if (is_int($options['post'])) {
				$options['post'] = get_post($options['post']);
				if (!is_object($options['post'])) {
					trigger_error(__("Function 'eswp_get_block_field', argument '\$options['post']', found no post object for value {$options['post']}."), E_USER_WARNING);
					return;
				}
			}
		}
		else if (isset($post)) {
			$options['post'] = $post;
		}
		if (isset($options['post'])) {
			$eswp_get_parsed_blocks_options['post'] = $options['post'];
		}

		// first_match
		if (isset($options['first_match'])) {
			if (!is_bool($options['first_match'])) {
				trigger_error(__("Function 'eswp_get_block_field', argument '\$options['first_match']' must be a boolean or unset."), E_USER_WARNING);
				return;
			}
		}
		else {
			$options['first_match'] = false;
		}

		// is_sub_field
		if (isset($options['is_sub_field'])) {
			if (!is_bool($options['is_sub_field'])) {
				trigger_error(__("Function 'eswp_get_block_field', argument '\$options['is_sub_field']' must be a boolean or unset."), E_USER_WARNING);
				return;
			}
		}
		else {
			$options['is_sub_field'] = false;
		}

		// return_field_object
		if (isset($options['return_field_object'])) {
			if (!is_bool($options['return_field_object'])) {
				trigger_error(__("Function 'eswp_get_block_field', argument '\$options['return_field_object']' must be a boolean or unset."), E_USER_WARNING);
				return;
			}
		}
		else {
			$options['return_field_object'] = false;
		}

		// parsed_blocks
		if (isset($options['parsed_blocks'])) {
			$eswp_get_parsed_blocks_options['parsed_blocks'] = $options['parsed_blocks'];
		}
		$options['parsed_blocks'] = eswp_get_parsed_blocks($eswp_get_parsed_blocks_options);

		foreach ($options['parsed_blocks'] as $parsed_block) {
			if (is_array($parsed_block['attrs']) && array_key_exists('data', $parsed_block['attrs'])) {

				$meta_id = eswp_setup_acf_block_meta($parsed_block);

				if ($options['is_sub_field']) {
					$field_value = get_sub_field($options['field'], $meta_id);
				}
				else {
					$field_value = get_field($options['field'], $meta_id);
				}

				if ($options['is_sub_field']) {
					$field_object = get_sub_field_object($options['field'], $meta_id);
				}
				else {
					$field_object = get_field_object($options['field'], $meta_id);
				}

				acf_reset_meta($meta_id);

				if (is_array($field_object) && is_string($field_object['name'])) {
					$field_name = $field_object['name'];
					if (isset($parsed_block['attrs']['data'][$field_name])) {
						if ($options['return_field_object'] === true) {
							$found_value = $field_object;
						}
						else {
							$found_value = $field_value;
						}
						if ($options['first_match']) {
							return $found_value;
						}
						else {
							array_push($found_values, $found_value);
						}
					}
				}

			}
		}

		if ($options['first_match']) {
			return null;
		}
		else {
			return $found_values;
		}
	}
}

if (!function_exists('eswp_get_block_field_object')) {
	function eswp_get_block_field_object($options) {
		$options['return_field_object'] = true;
		return eswp_get_block_field($options);
	}
}

if (!function_exists('eswp_get_block_sub_field')) {
	function eswp_get_block_sub_field($options) {
		$options['is_sub_field'] = true;
		return eswp_get_block_field($options);
	}
}
if (!function_exists('eswp_get_block_sub_field_object')) {
	function eswp_get_block_sub_field_object($options) {
		$options['is_sub_field'] = true;
		$options['return_field_object'] = true;
		return eswp_get_block_field($options);
	}
}


/**
 * eswp_format_link
 * 
 * Returns a HTML string of an <a> element constructed from an Advanced Custom Field link field.
 * 
 * @param  array  $link     Required. An ACF link field return array.
 * @param  array  $options  Optional. An array of options.
 * |- 'attributes'     =>  (array)   Optional. Attributes to add to the <a> element.
 * |- 'prepend_html'   =>  (string)  Optional. HTML to add after the opening <a> tag.
 * |- 'append_html'    =>  (string)  Optional. HTML to add before the closing <a> tag.
 * |- 'text_override'  =>  (string)  Optional. Text to override the link field text.
 * |- 'text_wrap'      =>  (array)   Optional. An array of options to wrap the link text.
 * |- |- 'html_tag'                =>  (string)   Required. The html tag to use for the wrapper.
 * |- |- 'attributes'              =>  (array)    Optional. Attributes to add the the wrapper element.
 * |- |- 'word_count'              =>  (int)      Optional. Number of words from start or end to include in the wrapper.
 * |- |- 'word_count_start_end'    =>  (string)   Optional. Set whether the word count should count from the start or end. Default: 'start'.
 * |- |- 'include_prepended_html'  =>  (boolean)  Optional. Include the prepended HTML in wrapper if the wrapper starts at the first word. Default: false.
 * |- |- 'include_appended_html'   =>  (boolean)  Optional. Include the appended HTML in wrapper if the wrapper ends at the last word. Default: false.
 * @return  string  The <a> element output.
 */

if (!function_exists('eswp_format_link')) {
	function eswp_format_link($link, $options = []) {
		if (!$link || !is_array($link)) {
			trigger_error(__("Function eswp_format_link expects 'link' argument to be an ACF link field with array return format."), E_USER_WARNING);
			return false;
		}
		if (!is_array($options)) {
			trigger_error(__("Function eswp_format_link expects 'options' argument to be an array or unset."), E_USER_WARNING);
			return false;
		}

		$link_attributes = [
			'href' => esc_url($link['url']),
			'target' => '_self',
		];
		if ($link['target']) {
			$link_attributes['target'] = esc_attr($link['target']);
		}
		if (array_key_exists('attributes', $options)) {
			if (!is_array($options['attributes'])) {
				trigger_error(__("Function eswp_format_link argument \$options['attributes'] must be an array of attributes and values or unset."), E_USER_WARNING);
				return false;
			}
			foreach ($options['attributes'] as $attribute => $value) {
				$link_attributes[$attribute] = $value;
			}
		}

		$link_text = esc_html($link['title']);
		if (array_key_exists('text_override', $options)) {
			if (!is_string($options['text_override'])) {
				trigger_error(__("Function eswp_format_link argument \$options['text_override'] must be a string or unset."), E_USER_WARNING);
				return false;
			}
			$link_text = __($options['text_override']);
		}

		$prepend_html = '';
		if (array_key_exists('prepend_html', $options)) {
			if (!is_string($options['prepend_html'])) {
				trigger_error(__("Function eswp_format_link argument \$options['prepend_html'] must be a HTML string or unset."), E_USER_WARNING);
				return false;
			}
			$prepend_html = $options['prepend_html'];
		}

		$append_html = '';
		if (array_key_exists('append_html', $options)) {
			if (!is_string($options['append_html'])) {
				trigger_error(__("Function eswp_format_link argument \$options['append_html'] must be a HTML string or unset."), E_USER_WARNING);
				return false;
			}
			$append_html = $options['append_html'];
		}

		$text_output = '';
		if (array_key_exists('text_wrap', $options)) {
			if (!is_array($options['text_wrap'])) {
				trigger_error(__("Function eswp_format_link argument \$options['text_wrap'] must be a an array or unset."), E_USER_WARNING);
				return false;
			}
			if (!array_key_exists('html_tag', $options['text_wrap'])) {
				trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['html_tag'] must be set if the text_wrap array exists."), E_USER_WARNING);
				return false;
			}

			if (!is_string($options['text_wrap']['html_tag'])) {
				trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['html_tag'] must be a string representing an HTML tag or unset."), E_USER_WARNING);
				return false;
			}
			$wrapper_html_tag = $options['text_wrap']['html_tag'];

			$wrapper_attributes = [];
			if (array_key_exists('attributes', $options['text_wrap'])) {
				if (!is_array($options['text_wrap']['attributes'])) {
					trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['attributes'] must be an array or unset."), E_USER_WARNING);
					return false;
				}
				$wrapper_attributes = $options['text_wrap']['attributes'];
			}

			$wrapper_word_count = null;
			if (array_key_exists('word_count', $options['text_wrap'])) {
				if (!is_int($options['text_wrap']['word_count']) || $options['text_wrap']['word_count'] < 1) {
					trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['word_count'] must be an integer greater than zero or unset."), E_USER_WARNING);
					return false;
				}
				$wrapper_word_count = $options['text_wrap']['word_count'];
			}

			$wrapper_word_count_start_end = 'start';
			if (array_key_exists('word_count_start_end', $options['text_wrap'])) {
				if ($options['text_wrap']['word_count_start_end'] !== 'start' && $options['text_wrap']['word_count_start_end'] !== 'end') {
					trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['word_count_start_end'] must be 'start' or 'end' or unset."), E_USER_WARNING);
					return false;
				}
				$wrapper_word_count_start_end = $options['text_wrap']['word_count_start_end'];
			}

			$wrapper_include_prepended_html = false;
			if (array_key_exists('include_prepended_html', $options['text_wrap'])) {
				if (!is_bool($options['text_wrap']['include_prepended_html'])) {
					trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['include_prepended_html'] must be a boolean or unset."), E_USER_WARNING);
					return false;
				}
				$wrapper_include_prepended_html = $options['text_wrap']['include_prepended_html'];
			}

			$wrapper_include_appended_html = false;
			if (array_key_exists('include_appended_html', $options['text_wrap'])) {
				if (!is_bool($options['text_wrap']['include_appended_html'])) {
					trigger_error(__("Function eswp_format_link argument \$options['text_wrap']['include_appended_html'] must be a boolean or unset."), E_USER_WARNING);
					return false;
				}
				$wrapper_include_appended_html = $options['text_wrap']['include_appended_html'];
			}

			$link_text_words = explode(' ', $link_text);

			$wrapper_start_index = null;
			$wrapper_end_index = null;
			if (is_int($wrapper_word_count)) {
				if ($wrapper_word_count_start_end === 'start') {
					$wrapper_start_index = 0;
					$wrapper_end_index = min($wrapper_word_count, count($link_text_words)) - 1;
				}
				else {
					$wrapper_start_index = max(count($link_text_words) - $wrapper_word_count, 0);
					$wrapper_end_index = count($link_text_words) - 1;
				}
			}

			foreach ($link_text_words as $key => $value) {

				if ($key !== 0) {
					$text_output .= ' ';
				}

				if ($key === 0 && (!$wrapper_include_prepended_html || ($wrapper_start_index !== 0))) {
					$text_output .= $prepend_html;
				}

				if ($key === $wrapper_start_index || (is_null($wrapper_start_index) && $key === 0)) {
					$text_output .= '<' . $wrapper_html_tag . eswp_output_attributes($wrapper_attributes) . '>';
					if ($key === 0 && $wrapper_include_prepended_html) {
						$text_output .= $prepend_html;
					}
				}

				$text_output .= $value;

				if ($key === $wrapper_end_index || (is_null($wrapper_end_index) && $key === count($link_text_words) - 1)) {
					if ($key === count($link_text_words) - 1 && $wrapper_include_appended_html) {
						$text_output .= $append_html;
					}
					$text_output .= '</' . $wrapper_html_tag . '>';
				}

				if ($key === count($link_text_words) - 1 && (!$wrapper_include_appended_html || $wrapper_end_index !== count($link_text_words) - 1)) {
					$text_output .= $append_html;
				}

			}

		}
		else {
			$text_output .= $prepend_html . $link_text . $append_html;
		}

		$link_output = '<a' . eswp_output_attributes($link_attributes) . '>' . $text_output . '</a>';
		return $link_output;
	}
}