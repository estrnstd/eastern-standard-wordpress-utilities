# Eastern Standard WordPress Utilities
A WordPress plugin containing a suite of helpful utilities.

## Installation
Copy the contents of the `eswp-utilities` directory from this repository into the plugins directory. The path should look like this: `/wp-content/plugins/eswp-utilities/eswp-utilities.php`.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Requirements

This plugin requires these plugins to be installed and activated:

* <a href="https://www.advancedcustomfields.com/pro" target="_blank">Advanced Custom Fields Pro (>=5.8))</a>

## Updates
This plugin can be updated from the WordPress dashboard, read about it in the "Updating Plugins" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Disabling Updates

This plugin can have it's dashboard updates disabled, read about it in the "Disabling Plugin Updates" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Usage

### Attribute Utilities

Functions:

* `eswp_attributes_has_class_name($attributes, $class_name)`
* `eswp_attributes_add_class_name($attributes, $class_name)`
* `eswp_attributes_add_class_names($attributes, $class_names)`
* `eswp_attributes_remove_class_name($attributes, $class_name)`
* `eswp_attributes_remove_class_names($attributes, $class_names)`
* `eswp_output_attributes($attributes)`

Refer to the `eswp-utilities.php` file for more information about parameters and return values.

Example input and output:
```php
$attributes = [
	'id' => 'example-id',
	'class' => 'example-class-name another-class',
	'data-custom' => 'some custom data'
];
$attributes = eswp_attributes_add_class_name($attributes, 'my-class');
$attributes = eswp_attributes_remove_class_name($attributes, 'another-class');
echo '<div' . eswp_output_attributes($attributes) . '></div>';
// Expected output:
// <div id="example-id" class="example-class-name my-class" data-custom="some custom data"></div>
```

### Image Utilities

Function: `eswp_define_image_sizes($image_sizes)`

This function uses the WordPress function `add_image_size`, you can read more about what the options do here: https://developer.wordpress.org/reference/functions/add_image_size/

`'pixel_densities'` is optional, but must be an array of numbers if set. For each defined pixel density, new image styles will be created with dimensions set from multiplying the base image style by the pixel density and appending a modifier to the name. An image style is always generated for a pixel density of 1, so it's not necessary to include 1 in the list of pixel densities.

It's helpful to include the aspect ratio in a comment.

Example usage:

```php
eswp_define_image_sizes([
	[
		'name' => 'example-wide-large', // 16:9
		'width' => 1600,
		'height' => 900,
		'crop' => false,
		'pixel_densities' => [1.5, 2, 3]
	],
	[
		'name' => 'example-wide-medium', // 16:9
		'width' => 1200,
		'height' => 765,
		'crop' => false,
		'pixel_densities' => [1.25, 1.5, 2]
	],
	[
		'name' => 'example-wide-small', // 16:9
		'width' => 800,
		'height' => 450,
		'crop' => false
	],
	[
		'name' => 'example-square-large', // 1:1
		'width' => 1000,
		'height' => 1000,
		'crop' => false
	],
	[
		'name' => 'example-square-small', // 1:1
		'width' => 500,
		'height' => 500,
		'crop' => false
	],
]);
/*
Expected created pixel density image styles:
example-wide-large: 1600×900
example-wide-large-1.5x: 2400×1350
example-wide-large-2x: 3200×1800
example-wide-large-3x: 4800×2700
example-wide-medium: 1200×765
example-wide-medium-1.25x: 1500×956
example-wide-medium-1.5x: 1800×1147
example-wide-medium-2x: 2400×1530
example-wide-small: 800×450
example-square-large: 1000x1000
example-square-small: 500x500
*/
```

- - -

Function: `eswp_format_acf_picture($sources, $options)`

**Note**: Using this function requires the Advanced Custom Fields (ACF) plugin to be installed and activated.

The order of the sources in the `$sources` array will dictate the order of the `<source>` elements within the picture element. This is important if you structure your media queries in a way that relies on a specific fallback order. You can read more about the HTML `<picture>` element here: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture.

One source in the `$sources` argument should have `'fallback' => true`. This will be the image chosen for the `<img>` tag. The image tag is present to account for browsers without `<picture>` element support. If no fallback is set manually, the first source in the `$sources` array will be used.

Empty fields will be replaced by SVGs of a missing image icon. The icon will be dynamically generated to match the desired image style dimensions (if one was defined) or will default to the dimensions of 500x500 pixels.

If `'pixel_densities'` is set on a source. The output will only include pixel densities when the image uploaded was big enough to meet or exceed the dimensions required for that density. Pixel densities should only include densities defined in `eswp_define_image_sizes` for that image size.

You can optionally define a `block_name` and/or `post` for any sources as well. These options allow you to display images from specific blocks and/or other posts.

You can also directly pass a field object into `field` instead of relying on the function to get the field object for you. This is helpful when referencing a field by non-standard means.

Refer to the function definition for more detailed documentation of inputs and output: https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities/src/main/eswp-utilities/eswp-utilities.php

The only currently available option is `$options['picture']['attributes']`. This option allows you to pass an array of attributes to be output with the `<picture>` element. Check the Attribute Utilities section of this readme for further information on how the attributes will be handled.

**Note**: If images are not displaying correctly (incorrect image sizes) try regenerating thumbnails using this plugin: https://wordpress.org/plugins/regenerate-thumbnails/. The relevant admin page can be found at Tools > Regenerate Thumbnails.

Example input and output:

```php
$picture_output = eswp_format_acf_picture([
	[
		'field' => 'image_field_2',
		'size' => 'example-square-small',
		'media' => '(max-width: 31.25rem)' // 500px
	],
	[
		'field' => 'image_field_1',
		'size' => 'example-wide-medium',
		'media' => '(max-width: 75rem)', // 1200px
		'pixel_densities' => [2],
		'fallback' => true
	],
	[
		'field' => 'image_field_1',
		'size' => 'example-wide-large',
		'pixel_densities' => [2, 3],
	]
]);
echo $picture_output;
// Expected output:
/*
<picture>
	<source srcset="/path/to/small-square-image.jpg" type="image/jpeg" media="(max-width: 31.25rem)">
	<source srcset="/path/to/medium-wide-image.jpg, /path/to/medium-wide-image-1.5x.jpg 1.5x" media="(max-width: 75rem)" type="image/jpeg">
	<source srcset="/path/to/large-image.jpg, /path/to/large-image-1.5x.jpg 1.5x, /path/to/large-image-2x.jpg 2x" type="image/jpeg">
	<img src="/path/to/medium-wide-image.jpg" alt="example alt text">
</picture>
*/
```

### Block Utilities

Function: `eswp_get_block_field($options)`

Returns an array of blocks and their values for the matching field defined in the options.

Accepts an `$options` argument with the following key value pairs:

* `'field'` (string) Required. The field key/name to get the value from.
* `'block_name'` (string) Optional. The name of the block containing the field. if omitted, all blocks will be included. This is helpful to eliminate false positive results from lingering metadata after the removal of a field.
* `'post'` (object) Optional. The post object or post id where to search for the values.
* `'first_match'` => (boolean) Optional. If true, will return the first found value directly.
* `'return_field_object'` (boolean) Optional. If true, will return the field object instead of the field value.

Will return an array containing blocks and the field values. Or if `first_match` is true, return the direct field or null if no field is found.

## Development

The versioning for this plugin follows the <a href="https://semver.org" target="_blank">semver</a> guidelines.

When committing updates, remember to update the version in `eswp-utilities.php` and `info.json`, then compress the `eswp-utilities` directory into a new `eswp-utilities.zip` file. These steps ensure the WordPress plugin dashboard update process will work as expected.